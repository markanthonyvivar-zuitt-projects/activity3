package com.vivar;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
	// write your code here


        User a = new User("Mark Anthony", "Vivar", "09212167236");

        User b = new User("Mary Grace", "Vivar", "09396242888");

        ArrayList<User> usersList = new ArrayList<>();
        usersList.add(a);
        usersList.add(b);

        for (User user : usersList) {
            System.out.println(user.display());

        }

    }
}
