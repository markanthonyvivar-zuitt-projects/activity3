package com.vivar;

public class User {

    private String firstName;
    private String lastName;
    private String contact;

    public User() {
        System.out.println("A new user object was created.");
    }

    public User (String newFirstName, String newLastName, String newContact) {
        System.out.println("A new user object was created.");
        this.firstName = newFirstName;
        this.lastName = newLastName;
        this.contact = newContact;
    }

    public String getFirstName() {
        return this.firstName;
    }
    public String getLastName() {
        return this.lastName;
    }
    public String getContact() {
        return this.contact;
    }

    public void setFirstName(String newFirstName) {
        this.firstName = newFirstName;
    }
    public void newLastName(String newLastName) {
        this.lastName = newLastName;
    }
    public void newContact(String newContact) {
        this.contact = newContact;
    }

    public String display() {
        return this.getFirstName() + " " + this.getLastName() + " " + this.getContact();
    }






}
